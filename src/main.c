#include <HGE/HGE_Core.h>

int main()
{
	hge_window window = {"Another Platformer", 800, 600};
	hgeInit(60, window, HGE_INIT_ALL);

	initGame();

	hgeStart();

	cleanUpGame();

	return 0;
}
