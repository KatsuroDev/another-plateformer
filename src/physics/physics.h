#ifndef CP_PHYSICS_H
#define CP_PHYSICS_H

#include <HGE/HGE_Core.h>
#include <chipmunk/chipmunk.h>

typedef struct {
    cpSpace* space;
} chipmunk_space;

typedef struct {
    cpShape* shape;
} chipmunk_shape;

typedef struct {
    cpBody* body;
} chipmunk_body;

void createPhysicWorld();
void SpaceSystem(hge_entity* e, chipmunk_space* cmS);


void addSpaceToWorld(cpSpace* space);
void addBodyToWorld(cpBody* body);
void addShapeToWorld(cpShape* shape);

void freeSpaces();
void freeBodies();
void freeShapes();

void freeAllPhysics();

#endif
