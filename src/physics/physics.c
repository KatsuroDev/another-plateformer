#include "physics.h"

cpSpace* spaces[10];
int numSpaces = 0;

cpBody* bodies[1000];
int numBodies = 0;

cpShape* shapes[1000];
int numShapes = 0;


void createPhysicWorld()
{
    chipmunk_space cmS = { cpSpaceNew() };
    cpVect gravity = cpv(0, -200);
    cpSpaceSetGravity(cmS.space, gravity);

    addSpaceToWorld(cmS.space);

    tag_component world_tag;
    hge_entity* world = hgeCreateEntity();
    hgeAddComponent(world, hgeCreateComponent("Chipmunk Space", &cmS, sizeof(cmS)));
    hgeAddComponent(world, hgeCreateComponent("Physic World", &world_tag, sizeof(world_tag)));

}

void SpaceSystem(hge_entity* e, chipmunk_space* cmS)
{
    for(int i = 0; i < 4; i++)
    {
        cpSpaceStep(cmS->space, hgeDeltaTime()/4);
    }
}

void addSpaceToWorld(cpSpace* space)
{
    spaces[numSpaces] = space;
    numSpaces++;
}

void addBodyToWorld(cpBody* body)
{
    bodies[numBodies] = body;
    numBodies++;
}

void addShapeToWorld(cpShape* shape)
{
    shapes[numShapes] = shape;
    numShapes++;
}

void freeSpaces()
{
    for(int i = 0; i < numSpaces; i++)
        cpSpaceFree(spaces[i]);
}

void freeBodies()
{
    for(int i = 0; i < numBodies; i++)
        cpBodyFree(bodies[i]);
}

void freeShapes()
{
    for(int i = 0; i < numShapes; i++)
        cpShapeFree(shapes[i]);
}

void freeAllPhysics()
{
    freeShapes();
    freeBodies();
    freeSpaces();

    numShapes = 0;
    numBodies = 0;
    numSpaces = 0;
}
