#ifndef AP_LEVEL_H
#define AP_LEVEL_H

#include <HGE/HGE_Core.h>
#include "map.h"
#include "../physics/physics.h"

ap_map LoadLevel(const char* level_path);
chipmunk_shape createBoxShape(chipmunk_space* cmS, chipmunk_body* cmB, hge_vec2 scale);



#endif
