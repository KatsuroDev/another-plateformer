#include "movingplatform.h"
#include "../utils/utils.h"

void createMovingPlatform(hge_transform transform)
{
    printf("Hello!\n");
    hge_entity* e = hgeCreateEntity();
    hge_transform mp_transform = transform;
    hgeAddComponent(e, hgeCreateComponent("Transform", &mp_transform, sizeof(mp_transform)));

    hge_entity* world = hgeQueryEntity(1, "Physic World");
    chipmunk_space* cmS = world->components[hgeQuery(world, "Chipmunk Space")].data;

    chipmunk_body cmB;
    cmB.body = cpSpaceAddBody(cmS->space, cpBodyNewKinematic());
    cpBodySetPosition(cmB.body, cpv(mp_transform.position.x, mp_transform.position.y));
    addBodyToWorld(cmB.body);
    hgeAddComponent(e, hgeCreateComponent("Chipmunk Body", &cmB, sizeof(cmB)));

    chipmunk_shape cmSp;
    cmSp.shape = cpSpaceAddShape(cmS->space, cpBoxShapeNew(cmB.body, mp_transform.scale.x, mp_transform.scale.y, 0));
    cpShapeSetFriction(cmSp.shape, 0);
    addShapeToWorld(cmSp.shape);
    hgeAddComponent(e, hgeCreateComponent("Chipmunk Shape", &cmSp, sizeof(cmSp)));

    movingPlatform_component data;
    data.timer = 0;
    hgeAddComponent(e, hgeCreateComponent("MovingPlatform", &data, sizeof(data)));

}

void MovingPlatformSystem(hge_entity* e, chipmunk_body* cmB, movingPlatform_component* data)
{
    data->timer += hgeDeltaTime();
    cpVect vel = {100, 0};
    vel.x *= sin(data->timer);
    cpBodySetVelocity(cmB->body, vel);


    hge_transform* sprite_transform = e->components[hgeQuery(e, "Transform")].data;
    sprite_transform->position.x = (sprite_transform->position.x - TILE*3) + TILE/2;
    sprite_transform->scale.x = TILE;
    sprite_transform->scale.y = TILE;

    hge_vec2 res = {16, 16};

    for(int i = 0; i < 6; i++)
    {
        int sprite_id = 172;
        if(i == 0) sprite_id = 172;
        else if(i == 5) sprite_id = 176;
        else sprite_id = 173;

        int x = sprite_id%(int)(512/res.x);
        int y = sprite_id/(int)(512/res.x);

        hge_vec2 frame = {x, y};

        hgeRenderSpriteSheet(hgeResourcesQueryShader("sprite_shader"), hgeResourcesQueryTexture("MainSheet"), sprite_transform->position, sprite_transform->scale, 0, res, frame);

        sprite_transform->position.x += TILE;
    }
}
