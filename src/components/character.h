#ifndef AP_CHARACTER_H
#define AP_CHARACTER_H

#include <HGE/HGE_Core.h>
#include "../physics/physics.h"

typedef enum {
    IDLE, MOVING, JUMPING // WE'LL ADD JUMPING I GUESS
} character_movement_state;

typedef struct {
    character_movement_state mov_state;
    hge_vec2 movement_direction;
    float walk_speed;
} character_component;

void CharacterSystem(hge_entity* e, hge_transform* transform, character_component* data);

#endif
