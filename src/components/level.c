#include "level.h"
#include "player.h"
#include "../utils/utils.h"
#include "../TMX/tmx.h"
#include "movingplatform.h"


chipmunk_shape createBoxShape(chipmunk_space* cmS, chipmunk_body* cmB, hge_vec2 scale)
{
    chipmunk_shape cmSp;
    cmSp.shape = cpSpaceAddShape(cmS->space, cpBoxShapeNew(cmB->body, scale.x, scale.y, 0));
    cpShapeSetFriction(cmSp.shape, 0);
    addShapeToWorld(cmSp.shape);
    return cmSp;
}

void ParseTMXObject(tmx_object* object, tmx_map* map, ap_map* level_map) {
    hge_entity* world = hgeQueryEntity(1, "Physic World");
    chipmunk_space* cmS = world->components[hgeQuery(world, "Chipmunk Space")].data;
    if(object->type)
    {
        if(strcmp(object->type, "wall") == 0)
        {

            hge_vec2 scl = {object->width, object->height};
            hge_vec2 pos = {object->x + object->width/2, -object->y - object->height/2};

            chipmunk_body cmB;
            cmB.body = cpSpaceAddBody(cmS->space, cpBodyNewStatic());
            cpBodySetPosition(cmB.body, cpv(pos.x, pos.y));
            addBodyToWorld(cmB.body);

            chipmunk_shape cmSp = createBoxShape(cmS, &cmB, scl);

            level_map->collisions.bodies[level_map->collisions.num_coll] = cmB;
            level_map->collisions.shapes[level_map->collisions.num_coll] = cmSp;
            level_map->collisions.num_coll++;

        }
        if(strcmp(object->type, "movingThing") == 0)
        {
            hge_transform transform;
            transform.position.x = object->x + object->width/2;
            transform.position.y = -object->y - object->height/2;
            transform.position.z = 0;
            transform.scale.x = object->width;
            transform.scale.y = object->height;
            transform.scale.z = 0;
            createMovingPlatform(transform);
        }
    }
    if(object->name)
    {
        if(strcmp(object->name, "PlayerSpawn") == 0)
        {
            hge_vec2 pos = {object->x, -object->y};
            printf("x: %f, y: %f\n", pos.x, pos.y);
            pos.x += 8*TILE;
            addCamera(pos);
            pos.x -= 8*TILE;
            createPlayer(pos.x, pos.y);

        }
        if(strcmp(object->name, "Win Point") == 0)
        {
            hge_entity* e = hgeCreateEntity();
            hge_transform transform;
            transform.position.x = object->x + object->width/2;
            transform.position.y = -object->y - object->height/2;
            transform.position.z = 0;
            transform.scale.x = object->width;
            transform.scale.y = object->height;
            transform.scale.z = 0;
            tag_component tag;
            hgeAddComponent(e, hgeCreateComponent("Transform", &transform, sizeof(transform)));
            hgeAddComponent(e, hgeCreateComponent("WinPoint", &tag, sizeof(tag)));
        }
    }
}

void ParseTMXData(tmx_map* map, ap_map* level_map)
{

    level_map->width = map->width;
    level_map->height = map->height;
    level_map->far = malloc(sizeof(int)*map->width*map->height);
    level_map->background = malloc(sizeof(int)*map->width*map->height);
    level_map->bottom_ground = malloc(sizeof(int)*map->width*map->height);
    level_map->ground = malloc(sizeof(int)*map->width*map->height);
    level_map->walls = malloc(sizeof(int)*map->width*map->height);
    level_map->decoration = malloc(sizeof(int)*map->width*map->height);
    level_map->clouds = malloc(sizeof(int)*map->width*map->height);
    level_map->grass = malloc(sizeof(int)*map->width*map->height);
    tmx_layer* layer = map->ly_head;
    while(layer != NULL) {
        if(strcmp(layer->name, "Sky") == 0) {
            createBackground();
        } else if(strcmp(layer->name, "far") == 0) {
            for(int i = 0; i < map->width*map->height; i++)
            {
                level_map->far[i] = layer->content.gids[i];
            }
        } else if (strcmp(layer->name, "background") == 0) {
            for(int i = 0; i < map->width*map->height; i++)
            {
                level_map->background[i] = layer->content.gids[i];
            }
        } else if (strcmp(layer->name, "bottom_ground") == 0) {
            for(int i = 0; i < map->width*map->height; i++)
            {
                level_map->bottom_ground[i] = layer->content.gids[i];
            }
        } else if (strcmp(layer->name, "ground") == 0) {
            for(int i = 0; i < map->width*map->height; i++)
            {
                level_map->ground[i] = layer->content.gids[i];
            }
        } else if (strcmp(layer->name, "walls") == 0) {
            for(int i = 0; i < map->width*map->height; i++)
            {
                level_map->walls[i] = layer->content.gids[i];
            }
        } else if (strcmp(layer->name, "decoration") == 0) {
            for(int i = 0; i < map->width*map->height; i++)
            {
                level_map->decoration[i] = layer->content.gids[i];
            }
        } else if (strcmp(layer->name, "clouds") == 0) {
            for(int i = 0; i < map->width*map->height; i++)
            {
                level_map->clouds[i] = layer->content.gids[i];
            }
        } else if (strcmp(layer->name, "grass") == 0) {
            for(int i = 0; i < map->width*map->height; i++)
            {
                level_map->grass[i] = layer->content.gids[i];
            }
        } else if (strcmp(layer->name, "fences") == 0) {
            for(int i = 0; i < map->width*map->height; i++)
            {
                if(layer->content.gids[i])
                    level_map->grass[i] = layer->content.gids[i];
            }
        } else if (strcmp(layer->name, "Collision") == 0) {
            tmx_object_group* object_group = layer->content.objgr;
            tmx_object* object = object_group->head;
            int number_object = 0;
            while(object) {
                number_object++;
                object = object->next;
            }
            level_map->collisions.num_coll = 0;
            level_map->collisions.bodies = malloc(sizeof(chipmunk_body)*number_object);

            level_map->collisions.shapes = malloc(sizeof(chipmunk_shape)*number_object);
            object = object_group->head;
            while(object) {
                ParseTMXObject(object, map, level_map);
                object = object->next;
            }
        } else if (strcmp(layer->name, "Trigger") == 0) {
            tmx_object_group* object_group = layer->content.objgr;
            tmx_object* object = object_group->head;
            while(object) {
                ParseTMXObject(object, map, level_map);
                object = object->next;
            }
        }
        layer = layer->next;
    }
}

ap_map LoadLevel(const char* level_path)
{
    ap_map map;
    printf("Loading Level '%s'\n", level_path);
    tmx_map *map_loading = tmx_load(level_path);
    if(!map_loading) {tmx_perror("Cannot load map"); return;}
    ParseTMXData(map_loading, &map);
    tmx_map_free(map_loading);
    return map;
}
