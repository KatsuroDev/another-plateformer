#include "player.h"
#include "../utils/utils.h"
#include <HGE/HGE_Core.h>
#include "../physics/physics.h"
#include "game.h"
#include "map.h"

void createPlayer(float x, float y)
{
    hge_entity* e = hgeCreateEntity();

    hge_transform transform;
    transform.position.x = x;
    transform.position.y = y;
    transform.position.z = 6;
    transform.scale.x = TILE;
    transform.scale.y = TILE;
    transform.scale.z = 0;
    hgeAddComponent(e, hgeCreateComponent("Transform", &transform, sizeof(transform)));

    character_component character_data;
    character_data.mov_state = IDLE;
    character_data.movement_direction.x = 0;
    character_data.movement_direction.y = 0;
    character_data.walk_speed = 100.0f;
    hgeAddComponent(e, hgeCreateComponent("Character", &character_data, sizeof(character_data)));

    hge_entity* world = hgeQueryEntity(1, "Physic World");
    chipmunk_space* cmS = world->components[hgeQuery(world, "Chipmunk Space")].data;

    cpFloat mass = 1;
    cpFloat moment = cpMomentForBox(mass, transform.scale.x, transform.scale.y);

    chipmunk_body cmB;
    cmB.body = cpSpaceAddBody(cmS->space, cpBodyNew(mass, moment));
    cpBodySetPosition(cmB.body, cpv(transform.position.x, transform.position.y));
    addBodyToWorld(cmB.body);
    hgeAddComponent(e, hgeCreateComponent("Chipmunk Body", &cmB, sizeof(cmB)));

    chipmunk_shape cmSp;
    cmSp.shape = cpSpaceAddShape(cmS->space, cpBoxShapeNew(cmB.body, transform.scale.x, transform.scale.y, 0));
    cpShapeSetFriction(cmSp.shape, 1);
    addShapeToWorld(cmSp.shape);
    hgeAddComponent(e, hgeCreateComponent("Chipmunk Shape", &cmSp, sizeof(cmSp)));

    hge_texture sprite = hgeResourcesQueryTexture("Hero_IDLE");
    hgeAddComponent(e, hgeCreateComponent("Sprite", &sprite, sizeof(sprite)));

    tag_component player_tag;
    hgeAddComponent(e, hgeCreateComponent("Player", &player_tag, sizeof(player_tag)));


    // FOLLOW
    follow_component follow;
    follow.lock_x = true;
    follow.lock_y = false;
    follow.lock_z = true;
    follow.speed = 10.f;

    hge_transform* follow_transform = e->components[hgeQuery(e, "Transform")].data;
    hge_vec3* player_position = &follow_transform->position;
    follow.target_pos = player_position;

    hge_entity* cam = hgeQueryEntity(1, "ActiveCamera");
    //follow_component* follow_data = cam->components[hgeQuery(cam, "Follow")].data;

    //follow_data->target_pos = player_position;
    hgeAddComponent(cam, hgeCreateComponent("Follow", &follow, sizeof(follow)));


    //tag_component playable_tag;
    //hgeAddComponent(e, hgeCreateComponent("Playable", &playable_tag, sizeof(playable_tag)));

}

void PlayableSystem(hge_entity* e, hge_transform* transform, character_component* data, tag_component* tag)
{

    hge_transform foot = *transform;
    foot.scale.x = foot.scale.x/1.5f;
    foot.position.y -= 5;

    hge_entity* map = hgeQueryEntity(1, "Map");
    ap_map* map_data = map->components[hgeQuery(map, "Map")].data;
    bool canJump = false;
    for(int i = 0; i < map_data->collisions.num_coll; i++)
    {
        hge_transform collision_transform;
        cpBody* body = map_data->collisions.bodies[i].body;
        cpVect pos = cpBodyGetPosition(body);
        cpBB box = cpShapeGetBB(map_data->collisions.shapes[i].shape);
        collision_transform.position.x = pos.x;
        collision_transform.position.y = pos.y;
        collision_transform.position.z = 10;
        collision_transform.scale.x = (box.r - pos.x) * 2;
        collision_transform.scale.y = (box.t - pos.y) * 2;
        collision_transform.scale.z = 0;
        if(AABB(foot, collision_transform))
            canJump = true;
    }
    hge_entity* movingPlatform = hgeQueryEntity(1, "MovingPlatform");
    chipmunk_body* movingBody = movingPlatform->components[hgeQuery(movingPlatform, "Chipmunk Body")].data;
    chipmunk_shape* movingShape = movingPlatform->components[hgeQuery(movingPlatform, "Chipmunk Shape")].data;

    hge_transform moving_transform;
    cpVect Movingpos = cpBodyGetPosition(movingBody->body);
    cpBB Movingbox = cpShapeGetBB(movingShape->shape);
    moving_transform.position.x = Movingpos.x;
    moving_transform.position.y = Movingpos.y;
    moving_transform.position.z = 10;
    moving_transform.scale.x = (Movingbox.r - Movingpos.x) * 2;
    moving_transform.scale.y = (Movingbox.t - Movingpos.y) * 2;
    moving_transform.scale.z = 0;

    if(AABB(foot, moving_transform))
        canJump = true;


    //cpBB jumbBox = cpBBNew();

    hge_vec2 direction_vector = {0, 0};

    bool RIGHT = hgeInputGetKey(HGE_KEY_A);
    bool LEFT = hgeInputGetKey(HGE_KEY_D);

    bool UP = hgeInputGetKey(HGE_KEY_W);
    bool DOWN = hgeInputGetKey(HGE_KEY_S);

    bool JUMP = hgeInputGetKeyDown(HGE_KEY_SPACE);

    //if(RIGHT || LEFT || UP || DOWN)
    if(RIGHT || LEFT)
        data->mov_state = MOVING;
    else
        data->mov_state = IDLE;
    if(data->mov_state != JUMPING && JUMP && canJump)
        data->mov_state = JUMPING;


    switch(data->mov_state)
    {
        case IDLE:
            direction_vector.x = 0;
            direction_vector.y = 0;
            break;
        case MOVING:
            if(RIGHT)
                direction_vector.x = -1;
            if(LEFT)
                direction_vector.x = 1;
            /*if(DOWN)
                direction_vector.y = -1;
            if(UP)
                direction_vector.y = 1;*/

            direction_vector = hgeMathVec2Normalize(direction_vector);
            data->movement_direction = direction_vector;
            break;
    }

    hge_entity* game_entity = hgeQueryEntity(1, "Game");
    game_component* game = game_entity->components[hgeQuery(game_entity, "Game")].data;

    if(!isTransformWithinCamFrustrum(*transform) && game->state == RUNNING)
    {
        printf("Frustrum\n");
        hge_entity* camera = hgeQueryEntity(1, "ActiveCamera");

        removeComponent(e, hgeQuery(e, "Playable"));
        removeComponent(camera, hgeQuery(camera, "CameraLevel"));
        data->mov_state = IDLE;
        game->state = LOOSE;
    }
    hge_entity* winPoint = hgeQueryEntity(1, "WinPoint");
    hge_transform* win_transform = winPoint->components[hgeQuery(winPoint, "Transform")].data;
    if(AABB(*transform, *win_transform) && game->state == RUNNING)
    {
        printf("AABB\n");
        hge_entity* player = hgeQueryEntity(1, "Player");
        hge_entity* camera = hgeQueryEntity(1, "ActiveCamera");

        removeComponent(player, hgeQuery(player, "Playable"));
        removeComponent(camera, hgeQuery(camera, "CameraLevel"));
        data->mov_state = IDLE;
        game->state = WIN;
    }


}
