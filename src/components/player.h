#ifndef AP_PLAYER_H
#define AP_PLAYER_H

#include <HGE/HGE_Core.h>
#include "../physics/physics.h"
#include "character.h"

void createPlayer(float x, float y);

void PlayableSystem(hge_entity* e, hge_transform* transform, character_component* data, tag_component* tag);

#endif
