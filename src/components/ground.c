#include "ground.h"
#include "../physics/physics.h"
#include "../utils/utils.h"


void createGround()
{

    hge_entity* e = hgeCreateEntity();

    hge_texture sprite = hgeResourcesQueryTexture("Plank");
    hge_transform transform;
    transform.position.y = 0;
    transform.position.z = 0;
    transform.scale.x = TILE;
    transform.scale.y = TILE;
    transform.scale.z = 0;

    transform.position.x = 0;
    hgeAddComponent(e, hgeCreateComponent("Transform", &transform, sizeof(transform)));
    hgeAddComponent(e, hgeCreateComponent("AP_Sprite", &sprite, sizeof(sprite)));



    hge_entity* whole_ground = hgeCreateEntity();

    hge_entity* world = hgeQueryEntity(1, "Physic World");
    chipmunk_space* cmS = world->components[hgeQuery(world, "Chipmunk Space")].data;

    chipmunk_body cmB;
    cmB.body = cpSpaceAddBody(cmS->space, cpBodyNewStatic());
    cpBodySetPosition(cmB.body, cpv(transform.position.x, transform.position.y));
    addBodyToWorld(cmB.body);
    hgeAddComponent(whole_ground, hgeCreateComponent("Chipmunk Body", &cmB, sizeof(cmB)));

    chipmunk_shape cmSp;
    //cmSp.shape = cpSpaceAddShape(cmS->space, cpSegmentShapeNew(cmB.body, cpv(-transform.scale.x/2, transform.scale.y/2), cpv(transform.scale.x/2, transform.scale.y/2), 0));
    cmSp.shape = cpSpaceAddShape(cmS->space, cpBoxShapeNew(cmB.body, transform.scale.x, transform.scale.y, 0));
    cpShapeSetFriction(cmSp.shape, 0);
    addShapeToWorld(cmSp.shape);
    hgeAddComponent(whole_ground, hgeCreateComponent("Chipmunk Shape", &cmSp, sizeof(cmSp)));



    //for(int i = -10; i < 10; i++)
    //{
        hge_entity* test = hgeCreateEntity();

        transform.position.x = 0;

        hgeAddComponent(e, hgeCreateComponent("Transform", &transform, sizeof(transform)));
        hgeAddComponent(e, hgeCreateComponent("AP_Sprite", &sprite, sizeof(sprite)));
    //}

}
