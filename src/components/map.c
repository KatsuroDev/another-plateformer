#include "map.h"
#include "level.h"


int* far_to_free = NULL;
int* background_to_free = NULL;
int* bottom_ground_to_free = NULL;
int* ground_to_free = NULL;
int* walls_to_free = NULL;
int* decoration_to_free = NULL;
int* clouds_to_free = NULL;
int* grass_to_free = NULL;
chipmunk_body* body_array_to_free = NULL;
chipmunk_shape* shape_array_to_free = NULL;


void RenderTile(hge_transform transform, int sprite_id)
{
    if(sprite_id && isTransformWithinCamFrustrum(transform))
    {
        sprite_id--;
        hge_vec2 res = {16, 16};
        hge_texture spritesheet = hgeResourcesQueryTexture("MainSheet");
        int x = sprite_id%(int)(spritesheet.width/res.x);
        int y = sprite_id/(int)(spritesheet.width/res.x);

        hge_vec2 frame = {x, y};
        hgeRenderSpriteSheet(hgeResourcesQueryShader("sprite_shader"), spritesheet, transform.position, transform.scale, 0, res, frame);
    }
}

void MapSystem(hge_entity* e, ap_map* map)
{
    for(int i = 0; i < map->width * map->height; i++)
    {
        int x = i%map->width;
        int y = i/map->width;

        hge_transform transform;
        transform.position.x = (x * TILE) + TILE/2;
        transform.position.y = (-y * TILE) - TILE/2;
        transform.position.z = 0;

        transform.scale.x = TILE;
        transform.scale.y = TILE;
        transform.scale.z = 0;

        RenderTile(transform, map->far[i]);
        transform.position.z++;
        RenderTile(transform, map->background[i]);
        transform.position.z++;
        RenderTile(transform, map->bottom_ground[i]);
        transform.position.z++;
        RenderTile(transform, map->ground[i]);
        transform.position.z++;
        RenderTile(transform, map->walls[i]);
        transform.position.z++;
        RenderTile(transform, map->decoration[i]);
        transform.position.z++;
        RenderTile(transform, map->clouds[i]);
        transform.position.z++;
        RenderTile(transform, map->grass[i]);

    }

    far_to_free = map->far;
    background_to_free = map->background;
    bottom_ground_to_free = map->bottom_ground;
    ground_to_free = map->ground;
    walls_to_free = map->walls;
    decoration_to_free = map->decoration;
    clouds_to_free = map->clouds;
    grass_to_free = map->grass;
}


void createMap()
{
    hge_entity* e = hgeCreateEntity();
    ap_map map_data = LoadLevel("./res/map.tmx");
    hgeAddComponent(e, hgeCreateComponent("Map", &map_data, sizeof(map_data)));
}

void freeTiles()
{
    free(far_to_free);
    free(background_to_free);
    free(bottom_ground_to_free);
    free(ground_to_free);
    free(walls_to_free);
    free(decoration_to_free);
    free(clouds_to_free);
    free(grass_to_free);
    free(body_array_to_free);
    free(shape_array_to_free);
}
