#ifndef AP_MAP_H
#define AP_MAP_H

#include <HGE/HGE_Core.h>
#include "../utils/utils.h"
#include "../physics/physics.h"

typedef struct {
    int num_coll;
    chipmunk_shape* shapes;
    chipmunk_body* bodies;
} ap_collisions;


typedef struct {
    int* far;
    int* background;
    int* bottom_ground;
    int* ground;
    int* walls;
    int* decoration;
    int* clouds;
    int* grass;
    int width;
    int height;
    ap_collisions collisions;
} ap_map;

void MapSystem(hge_entity* e, ap_map* map);

void createMap();

void freeTiles();

#endif
