#include "character.h"
#include "../utils/utils.h"


void CharacterMovement(hge_entity* e, hge_transform* transform, character_component* data)
{

    chipmunk_body* cmB = e->components[hgeQuery(e, "Chipmunk Body")].data;

    cpVect vel = cpBodyGetVelocity(cmB->body);
    float acceleration = 10.0f;
    switch(data->mov_state)
    {
        case IDLE:
            vel.x += (0 - vel.x) * acceleration * hgeDeltaTime();
            //vel.y += (0 - vel.y) * acceleration * hgeDeltaTime();
            break;
        case MOVING:
            if(data->movement_direction.x < 0)
                transform->scale.x = -TILE;
            if(data->movement_direction.x > 0)
                transform->scale.x = TILE;
            vel.x += (data->movement_direction.x*data->walk_speed - vel.x) * acceleration * hgeDeltaTime();
            //vel.y += (data->movement_direction.y*data->walk_speed - vel.y) * acceleration * hgeDeltaTime();
            break;
        case JUMPING:
            if(data->movement_direction.x < 0)
                transform->scale.x = -TILE;
            if(data->movement_direction.x > 0)
                transform->scale.x = TILE;
            vel.x += (data->movement_direction.x*data->walk_speed - vel.x) * acceleration * hgeDeltaTime();
            if(vel.y != 10)
                vel.y += 150.0f;
    }
    cpBodySetVelocity(cmB->body, vel);

}

void CharacterPhysics(hge_entity* e, hge_transform* transform, character_component* data)
{
}

void CharacterSystem(hge_entity* e, hge_transform* transform, character_component* data)
{
    CharacterMovement(e, transform, data);
    CharacterPhysics(e, transform, data);
}
