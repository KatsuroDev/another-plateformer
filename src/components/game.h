#ifndef AP_GAME_H
#define AP_GAME_H

#include <HGE/HGE_Core.h>


typedef enum {
    NONE,
    LOAD,
    START,
    RUNNING,
    WIN,
    LOOSE,
    RESTART
} game_state;

typedef struct {
    game_state state;
} game_component;

void createGame();
void GameSystem(hge_entity* e, game_component* game);

#endif
