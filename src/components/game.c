#include "game.h"
#include "../utils/utils.h"

void createGame()
{
    hge_entity* e = hgeCreateEntity();

    game_component game;
    game.state = NONE;

    hgeAddComponent(e, hgeCreateComponent("Game", &game, sizeof(game)));
}

void restartGame()
{
    createGame();
    hge_entity* game_entity = hgeQueryEntity(1, "Game");
    game_component* game_data = game_entity->components[hgeQuery(game_entity, "Game")].data;

    createPhysicWorld();

    createMap();

    game_data->state = LOAD;
}

void GameSystem(hge_entity* e, game_component* game)
{
    if(game->state == NONE)
    {
        printf("1\n");
        bool SPACE = hgeInputGetKeyDown(HGE_KEY_SPACE);
        if(SPACE)
        {
            createPhysicWorld();

            createMap();
            game->state = LOAD;
        }

    }else if (game->state == LOAD)
    {
        printf("2\n");

        bool SPACE = hgeInputGetKeyDown(HGE_KEY_SPACE);
        if(SPACE)
        {
            game->state = START;
        }
    }else if(game->state == START)
    {

        hge_entity* player = hgeQueryEntity(1, "Player");
        tag_component player_tag;
        hgeAddComponent(player, hgeCreateComponent("Playable", &player_tag, sizeof(player_tag)));

        hge_entity* camera = hgeQueryEntity(1, "ActiveCamera");
        tag_component cam_tag;
        hgeAddComponent(camera, hgeCreateComponent("CameraLevel", &cam_tag, sizeof(cam_tag)));

        game->state = RUNNING;
    }
    else if(game->state == WIN)
    {


        bool ENTER = hgeInputGetKeyDown(HGE_KEY_ENTER);
        if(ENTER)
        {
            game->state = RESTART;
        }
    }
    else if(game->state == LOOSE)
    {


        bool ENTER = hgeInputGetKeyDown(HGE_KEY_ENTER);
        if(ENTER)
        {
            game->state = RESTART;
        }
    }
    else if(game->state == RESTART)
    {
        
        hgeECSCleanUp();
        cleanUpGame();
        restartGame();
        return;
    }

}
