#ifndef AP_MOVINGPLATFORM_H
#define AP_MOVINGPLATFORM_H

#include <HGE/HGE_Core.h>
#include "../physics/physics.h"

typedef struct {
    float timer;
} movingPlatform_component;

void createMovingPlatform(hge_transform transform);

void MovingPlatformSystem(hge_entity* e, chipmunk_body* cmB, movingPlatform_component* data);
#endif
