#include <HGE/HGE_Core.h>
#include "utils.h"
#include "../physics/physics.h"
#include "components/character.h"
#include "components/player.h"
#include "components/ground.h"
#include "components/map.h"
#include "components/movingplatform.h"
#include "components/game.h"

void backgroundRendering(hge_entity* e, tag_component* tag)
{
    hgeClearColor(0.831, 0.941, 0.965, 1);
    hgeClear(0x00004000);
}

void CameraLevelMovingSystem(hge_entity* e, hge_vec3* position, tag_component* tag)
{
    position->x += 3*TILE * hgeDeltaTime();
}

void loadTextures()
{
    hgeResourcesLoadTexture("./res/tex/idle_hero.png", "Hero_IDLE");
    hgeResourcesLoadTexture("./res/tex/plank.png", "Plank");
    //hgeResourcesLoadTexture("./res/tex/blue.png", "Hero_IDLE");
    //hgeResourcesLoadTexture("./res/tex/green.png", "Plank");
    hgeResourcesLoadTexture("./res/tex/ap_sheet.png", "MainSheet");
}

bool isTransformWithinCamFrustrum(hge_transform transform)
{
    hge_entity* cam = hgeQueryEntity(1, "ActiveCamera");
    hge_camera* camera_data = cam->components[hgeQuery(cam, "Camera")].data;
    hge_vec3* cam_pos = cam->components[hgeQuery(cam, "Position")].data;

    hge_transform camera_transform;
    camera_transform.position = *cam_pos;
    camera_transform.scale.x = hgeWindowWidth() * camera_data->fov;
    camera_transform.scale.y = hgeWindowHeight() * camera_data->fov;

    return AABB(transform, camera_transform);
}

void AP_SpriteRenderingSystem(hge_entity* entity, hge_transform* transform, hge_texture* sprite) {

    if(isTransformWithinCamFrustrum(*transform))
        hgeRenderSprite(hgeResourcesQueryShader("sprite_shader"), *sprite, transform->position, transform->scale, 0.0f);
}

void SyncCMBwTransform(hge_entity* e, hge_transform* transform, chipmunk_body* cmB) {
    cpVect chipmunk_pos = cpBodyGetPosition(cmB->body);
    transform->position.x = chipmunk_pos.x;
    transform->position.y = chipmunk_pos.y;
}

void addAllSystems()
{
    // Add background here before all of them
    hgeAddSystem(backgroundRendering, 1, "bg");
    hgeAddBaseSystems();
    hgeAddSystem(SyncCMBwTransform, 2, "Transform", "Chipmunk Body");
    hgeAddSystem(AP_SpriteRenderingSystem, 2, "Transform", "AP_Sprite");
    hgeAddSystem(SpaceSystem, 1, "Chipmunk Space");
    hgeAddSystem(CharacterSystem, 2, "Transform", "Character");
    hgeAddSystem(MovingPlatformSystem, 2, "Chipmunk Body", "MovingPlatform");
    hgeAddSystem(PlayableSystem, 3, "Transform", "Character", "Playable");
    hgeAddSystem(MapSystem, 1, "Map");
    hgeAddSystem(CameraLevelMovingSystem, 2, "Position", "CameraLevel");
    hgeAddSystem(GameSystem, 1, "Game");
}

void addCamera(hge_vec2 pos)
{
    hge_entity* camera_entity = hgeCreateEntity();
    hge_camera cam = {true, true, 1.f/4.f, hgeWindowWidth(), hgeWindowHeight(), (float)hgeWindowWidth()/(float)hgeWindowHeight(), NEAR-1.0f, FAR+1.0f };
    hge_vec3 camera_position = {pos.x, pos.y, 0};
    orientation_component camera_orientation = {0.f, -90.0f, 0.f};
    hgeAddComponent(camera_entity, hgeCreateComponent("Camera", &cam, sizeof(cam)));
    hgeAddComponent(camera_entity, hgeCreateComponent("Position", &camera_position, sizeof(camera_position)));
    hgeAddComponent(camera_entity, hgeCreateComponent("Orientation", &camera_orientation, sizeof(camera_orientation)));
    tag_component activecam_tag;
    hgeAddComponent(camera_entity, hgeCreateComponent("ActiveCamera", &activecam_tag, sizeof(activecam_tag)));
    freemove_component freeCam;
    freeCam.speed = 500.f;
    //hgeAddComponent(camera_entity, hgeCreateComponent("FreeMove", &freeCam, sizeof(freeCam)));
}

void createBackground()
{
    hge_entity* e = hgeCreateEntity();
    tag_component tag;
    hgeAddComponent(e, hgeCreateComponent("bg", &tag, sizeof(tag)));
}

void initGame()
{
    loadTextures();
    addAllSystems();

    createGame();
    //createPhysicWorld();

    //createMap();
}

void removeComponent(hge_entity* entity, int index) {
  free(entity->components[index].data);
  for(int i = index; i < entity->numComponents; i++) {
    entity->components[i] = entity->components[i+1];
  }
  entity->numComponents--;
}

void cleanUpGame()
{
    freeAllPhysics();
    freeTiles();
}
