#ifndef AP_UTILS_H
#define AP_UTILS_H

#include <HGE/HGE_Core.h>
#include <chipmunk/chipmunk.h>
#include "../TMX/tmx.h"

#define TILE 16

#define NEAR -100.0f
#define FAR 100.0f


void loadTextures();

void addAllSystems();

void addCamera(hge_vec2 pos);

bool isTransformWithinCamFrustrum(hge_transform transform);

void createBackground();

void initGame();

void removeComponent(hge_entity* entity, int index);

void cleanUpGame();

#endif
