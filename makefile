LOCAL_ALLOW_UNDEFINED_SYMBOLS := true

TARGET_EXEC ?= a

BUILD_DIR ?= build
SRC_DIR ?= src

BUILD_OS ?= Linux


SRCS := $(shell find $(SRC_DIR) -name *.c)
OBJS := $(SRCS:%=$(BUILD_DIR)/%.o)
DEPS := $(OBJS:.o=.d)

INC_DIRS := $(shell find $(SRC_DIR) -type d)
INC_FLAGS := $(addprefix -I,$(INC_DIRS))


ifeq ($(BUILD_OS), Linux)
TARGET_EXEC := $(TARGET_EXEC).out
COMPILER ?= gcc
LINKER_FLAGS = $(INC_FLAGS) -L ./lib/linux/ -I ./inc/ -lhypergameengine -lchipmunk -lxml2 -lm -g
endif
ifeq ($(BUILD_OS), macos)
TARGET_EXEC := $(TARGET_EXEC).out
COMPILER ?= gcc
LINKER_FLAGS = $(INC_FLAGS) -L ./lib/MacOS/ -L /usr/local/lib/ -I ./inc/ -lhypergameengine -lchipmunk -lxml2
endif
ifeq ($(BUILD_OS), Win32)
TARGET_EXEC := $(TARGET_EXEC).exe
COMPILER ?= i686-w64-mingw32-gcc
LINKER_FLAGS = $(INC_FLAGS) -L ./lib/windows/i686/ -I ./inc/ -lhypergameengine -lchipmunk -lxml2 -lm -g
endif


$(BUILD_DIR)/$(TARGET_EXEC): $(OBJS)
	$(COMPILER) $(OBJS) -o $@ $(LDFLAGS) $(LINKER_FLAGS)

$(BUILD_DIR)/%.c.o: %.c
	$(MKDIR_P) $(dir $@)
	$(COMPILER) $(CFLAGS) $(LINKER_FLAGS) -c $< -o $@

	@echo "\nBuild Successful !\n"

.PHONY: linux
.PHONY: win32
.PHONY: clean

linux:
	@echo "--------------------"
	@echo "Compiling For Linux!"
	@echo "--------------------"
	make BUILD_OS=Linux

win32:
	@echo "--------------------"
	@echo "Compiling For Win32!"
	@echo "--------------------"
	make BUILD_OS=Win32

macos:
	@echo "--------------------"
	@echo "Compiling For MacOS!"
	@echo "--------------------"
	make BUILD_OS=macos

clean:
	$(RM) -r $(BUILD_DIR)/$(SRC_DIR)

-include $(DEPS)

MKDIR_P ?= mkdir -p
